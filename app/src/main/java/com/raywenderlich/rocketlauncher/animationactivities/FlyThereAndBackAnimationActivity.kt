package com.raywenderlich.rocketlauncher.animationactivities

import android.animation.ValueAnimator

class FlyThereAndBackAnimationActivity : BaseAnimationActivity() {
  override fun onStartAnimation() {
    // Create an animator
    val animator = ValueAnimator.ofFloat(0f, -screenHeight)

    animator.addUpdateListener {
      val value = it.animatedValue as Float
      rocket.translationY = value
      doge.translationY = value
    }

    // You can set the repeatMode to either of the following:
    // RESTART — restarts the animation from the beginning.
    // REVERSE — reverses the animation’s direction with every iteration.
    animator.repeatMode = ValueAnimator.REVERSE
    // do it 3x
    animator.repeatCount = 3

    // Set a duration and start the animation
    animator.duration = 500L
    animator.start()
  }
}
