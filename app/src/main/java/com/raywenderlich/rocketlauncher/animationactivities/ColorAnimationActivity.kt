package com.raywenderlich.rocketlauncher.animationactivities

import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.support.v4.content.ContextCompat
import com.raywenderlich.rocketlauncher.R

class ColorAnimationActivity : BaseAnimationActivity() {
  override fun onStartAnimation() {
    // Call ObjectAnimator.ofObject() and give it the following arguments:
    val objectAnimator = ObjectAnimator.ofObject(
            // frameLayout — the object with the property to be animated
            frameLayout,
            // "backgroundColor" — the property you want to animate
            "backgroundColor",
            // ArgbEvaluator() — an additional argument that specifies how to interpolate between two different ARGB (alpha, red, green, blue) color values
            ArgbEvaluator(),
            // Start and end color values — here you make use of ComtextCompat.getColor() to get the color resource id of a custom color specified in your colors.xml.
            ContextCompat.getColor(this, R.color.background_from),
            ContextCompat.getColor(this, R.color.background_to)
    )

    // Set the number of times the animation will repeat by setting the object’s repeatCount value.
    // Then you set its repeatMode to define what the animation does when it reaches the end.
    // More on this soon!
    objectAnimator.repeatCount = 1
    objectAnimator.repeatMode = ValueAnimator.REVERSE

    // Set duration and start the animation
    objectAnimator.duration = BaseAnimationActivity.Companion.DEFAULT_ANIMATION_DURATION
    objectAnimator.start()
  }
}
