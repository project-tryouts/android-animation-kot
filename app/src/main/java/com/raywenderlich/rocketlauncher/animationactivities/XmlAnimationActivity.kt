package com.raywenderlich.rocketlauncher.animationactivities

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import com.raywenderlich.rocketlauncher.R

class XmlAnimationActivity : BaseAnimationActivity() {
  override fun onStartAnimation() {
    // First, you load AnimatorSet from R.animator.jump_and_blink file, just like you normally would to inflate a view layout
    val rocketAnimatorSet = AnimatorInflater.loadAnimator(this, R.animator.jump_and_blink) as AnimatorSet
    // set rocket as the target for just-loaded animator
    rocketAnimatorSet.setTarget(rocket)

    // Load the animator from the same file once again
    val dogeAnimatorSet = AnimatorInflater.loadAnimator(this, R.animator.jump_and_blink) as AnimatorSet
    // Rinse and repeat for doge object

    dogeAnimatorSet.setTarget(doge)

    // Now you create a third AnimatorSet and set it up to play the first two simultaneously
    val bothAnimatorSet = AnimatorSet()
    bothAnimatorSet.playTogether(rocketAnimatorSet, dogeAnimatorSet)
    // Set the duration for the root animator and start
    bothAnimatorSet.duration = BaseAnimationActivity.Companion.DEFAULT_ANIMATION_DURATION
    bothAnimatorSet.start()
  }
}
