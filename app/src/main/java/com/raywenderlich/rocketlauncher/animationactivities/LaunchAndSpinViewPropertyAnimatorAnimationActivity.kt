package com.raywenderlich.rocketlauncher.animationactivities

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator

class LaunchAndSpinViewPropertyAnimatorAnimationActivity : BaseAnimationActivity() {
  override fun onStartAnimation() {
    rocket.animate()
            .translationY(-screenHeight)
            .rotationBy(360f)
            .setDuration(BaseAnimationActivity.Companion.DEFAULT_ANIMATION_DURATION)
            .start()


  }
}
