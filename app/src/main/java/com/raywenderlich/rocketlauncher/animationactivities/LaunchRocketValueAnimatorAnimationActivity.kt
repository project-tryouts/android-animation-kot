package com.raywenderlich.rocketlauncher.animationactivities

import android.animation.ValueAnimator
import android.view.animation.LinearInterpolator

class LaunchRocketValueAnimatorAnimationActivity : BaseAnimationActivity() {
  override fun onStartAnimation() {
    // Create an instance of ValueAnimator by calling the static method ofFloat.
    // It accepts the floating point numbers that’ll apply to the specified property of the animated object over time.
    // In this case, the values start at 0f and end with -screenHeight. Android starts screen coordinates at the top-left corner,
    // so the rocket’s Y translation changes from 0 to the negative of the screen height — it moves bottom to top.
    val valueAnimator = ValueAnimator.ofFloat(0f, -screenHeight)

    // Call addUpdateListener() and pass in a listener. ValueAnimator calls this listener with
    // every update to the animated value — remember the default delay of 10 ms.
    valueAnimator.addUpdateListener {
      // Get the current value from the animator and cast it to float; current value type is float because you created the ValueAnimator with ofFloat.
      val value = it.animatedValue as Float
      // Change the rocket’s position by setting its translationY value
      rocket.translationY = value
    }

    // Set up the animator’s duration and interpolator.
    valueAnimator.interpolator = LinearInterpolator()
    valueAnimator.duration = BaseAnimationActivity.Companion.DEFAULT_ANIMATION_DURATION

    // Start the animation.
    valueAnimator.start()
  }
}
