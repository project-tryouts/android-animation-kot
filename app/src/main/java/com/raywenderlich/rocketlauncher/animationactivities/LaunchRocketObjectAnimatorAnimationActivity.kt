package com.raywenderlich.rocketlauncher.animationactivities

import android.animation.ObjectAnimator

class LaunchRocketObjectAnimatorAnimationActivity : BaseAnimationActivity() {
  override fun onStartAnimation() {
    // Creating an instance of ObjectAnimator (like you did with ValueAnimator) except that the former takes two more parameters:
    // - rocket is the object to animate
    // - The object must have a property corresponding to the name of the property you wish to change,
    //   which in this example is “translationY”. You’re able to do this because rocket is an
    //   object of class View, which, in its base Java class, has an accessible setter with setTranslationY().
    val objectAnimator = ObjectAnimator.ofFloat(rocket, "translationY", 0f, -screenHeight)

    // set the duration for the animation and start it.
    objectAnimator.duration = BaseAnimationActivity.Companion.DEFAULT_ANIMATION_DURATION
    objectAnimator.start()
  }
}
