package com.raywenderlich.rocketlauncher.animationactivities

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator

class LaunchAndSpinAnimatorSetAnimationActivity : BaseAnimationActivity() {
  override fun onStartAnimation() {
    // Create a new ValueAnimator.
    val positionAnimator = ValueAnimator.ofFloat(0f, -screenHeight)

    // Attach an AnimatorUpdateListener to the ValueAnimator that updates the rocket’s position.
    positionAnimator.addUpdateListener {
      val value = it.animatedValue as Float
      rocket.translationY = value
    }

    // Create an ObjectAnimator, a second animator that updates the rocket’s rotation.
    val rotationAnimator = ObjectAnimator.ofFloat(rocket, "rotation", 0f, 180f)
    // Create a new instance of AnimatorSet.
    val animatorSet = AnimatorSet()
    // Specify that you’d like to execute positionAnimator together with rotationAnimator.
    animatorSet.play(positionAnimator).with(rotationAnimator)
    // Just as with a typical animator, you set a duration and call start().
    animatorSet.duration = BaseAnimationActivity.Companion.DEFAULT_ANIMATION_DURATION
    animatorSet.start()
  }
}
