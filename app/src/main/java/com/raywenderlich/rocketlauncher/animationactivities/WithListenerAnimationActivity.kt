package com.raywenderlich.rocketlauncher.animationactivities

import android.animation.Animator
import android.animation.ValueAnimator
import android.widget.Toast

class WithListenerAnimationActivity : BaseAnimationActivity() {
  override fun onStartAnimation() {
    // Create and set up an animator. You use ValueAnimator to change the position of
    // two objects simultaneously — you can’t do the same thing with a single ObjectAnimator.
    val animator = ValueAnimator.ofFloat(0f, -screenHeight)

    animator.addUpdateListener {
      val value = it.animatedValue as Float
      rocket.translationY = value
      doge.translationY = value
    }

    // Add the AnimatorListener.
    animator.addListener(object : Animator.AnimatorListener {
      override fun onAnimationStart(animation: Animator) {
        // Show a toast message when the animation starts
        Toast.makeText(applicationContext, "Doge took off", Toast.LENGTH_SHORT)
                .show()
      }

      override fun onAnimationEnd(animation: Animator) {
        // another toast when it ends
        Toast.makeText(applicationContext, "Doge is on the moon", Toast.LENGTH_SHORT)
                .show()
        finish()
      }

      override fun onAnimationCancel(animation: Animator) {}

      override fun onAnimationRepeat(animation: Animator) {}
    })

    // Start the animation as usual
    animator.duration = 5000L
    animator.start()
  }
}
